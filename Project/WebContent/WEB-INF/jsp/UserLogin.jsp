<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">

<link href="css/userLogin.css" rel="stylesheet" type="text/css" />
</head>

<body>



<h1 align="center">ログイン画面</h1><br>
<br>
<br>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


	<form action="UserLoginServlet" method="post">

		<div class= login-area>

			<div class="row">
				<b class="col-4">ログインID</b>
				<input type="text" name="loginId" class="form-control col-8" required><br>
			</div>

			<div class="row">
				<b class="col-4">パスワード</b>
				<input type="password" name="password" class="form-control col-8" required><br>
			</div>

		</div>

<br>
<br>

		<div class="button-area">
			<input type="submit" class="btn btn-primary" value="ログイン" >
		</div>
	</form>
</body>
</html>