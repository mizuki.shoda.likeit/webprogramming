<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー覧</title>
</head>

<body>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">

<link href="css/userTable.css" rel="stylesheet" type="text/css" />

<div class=text-area>
${userInfo.name}さん
<a href="UserLogoutServlet"><font color="red">ログアウト</font></a>
</div>

<h1 align="center">ユーザー覧</h1><br>

<div class=userTable-area>
	<div class=text-area>
	<a href="UserSignupServlet"><font color="blue">新規登録</font></a>
	</div>
	<br>
	<br>

	<form action="UserTableServlet" method="post">
	<div class="row">
		<b class=col-4>ログインID</b>
		<input type="text" name="loginId" class="form-control col-8"><br>
	</div>

	<div class="row">
		<b class=col-4>ユーザー名さん</b>
		<input type="text" name="userName" class="form-control col-8"><br>
	</div>

	<div class="row">
		<b class=col-4>生年月日</b>
		<input type="date" name="startBirthDate" class="form-control col-4">~
		<input type="date" name="endBirthDate" class="form-control col-3"><br>
	</div>

	<br>

	<div class=text-area>
	<input type="submit" class="btn btn-primary btn-lg" value="  検索  ">
	</div>
	</form>
</div>

<br>

<hr width="100%">

	<div class=userTable-area>

	<table border="1">
	<tr>
		<th>ログインID</th>
		<th>ユーザー名</th>
		<th>生年月日</th>
		<th></th>
	</tr>

		<c:forEach var="user" items="${userList}">
	<tr>


		<td>${user.loginId}</td>
		<td>${user.name}</td>
		<td>${user.birthDate}</td>

		<td>

		<a href="UserReferenceServlet?id=${user.id}" class="btn btn-primary">参照</a>

		<c:choose>
			<c:when test="${userInfo.loginId=='admin'}">
				<a href="UserUpdateServlet?id=${user.id}" class="btn btn-success">更新</a>
			</c:when>

			<c:when test="${userInfo.loginId == user.loginId}">
				<a href="UserUpdateServlet?id=${user.id}" class="btn btn-success">更新</a>
			</c:when>
		</c:choose>

		<c:if test="${userInfo.loginId=='admin'}">
			<a href="UserDeleteServlet?id=${user.id}" class="btn btn-danger">削除</a>
		</c:if>
		</td>
	</tr>
		</c:forEach>


	</table>
	</div>

</body>
</html>