<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
</head>
<body>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">

<link href="css/userUpdate.css" rel="stylesheet" type="text/css" />

<div class=text-area>
${userInfo.name}さん
<a href="UserLogoutServlet"><font color="red">ログアウト</font></a>
</div>

<h1 align="center">ユーザー情報更新</h1><br>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


<form action="UserUpdateServlet" method="post">
<div class=update-area>
	<div class="row">
		<b class="col-4">ログインID</b><span class="space">${userData.loginId}</span>
	</div>

	<div class="row">
		<b class="col-4">パスワード</b>
		<input type="text" name="password" class="form-control col-8">
	</div>

	<div class="row">
		<b class="col-4">パスワード(確認)</b>
		<input type="password" name=confirmPassword class="form-control col-8">
	</div>

	<div class="row">
		<b class="col-4">ユーザー名</b>
		<input type="text" class="form-control col-8" name="userName"  value="${userData.name}" required>
	</div>

	<div class="row">
		<b class="col-4">生年月日</b>
		<input type="date" class="form-control col-8" name="birthDate" value="${userData.birthDate}" required>

	<input type="hidden" name="id" value="${userData.id}">

	</div>
</div>

<br>
<br>
	<div class=button-area>
	<input type="submit" class="btn btn-primary" value="更新">
	</div>
</form>
<br>
<br>
<br>
<a href="UserTableServlet"><font color="blue">戻る</font></a>

</body>
</html>