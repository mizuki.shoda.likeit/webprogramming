<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報詳細参照</title>
</head>
<body>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">

<link href="css/userReference.css" rel="stylesheet" type="text/css" />

<div class=text-area>
${userInfo.name}さん
<a href="UserLogoutServlet"><font color="red">ログアウト</font></a>
</div>


<h1 align="center">ユーザー情報詳細参照</h1><br>

<div class=reference-area>

	<div class="row">
		<b>ログインID</b> <span class="space">${userData.loginId}</span>
	</div>

	<div class="row">
		<b>ユーザー名</b><span class="space">${userData.name}</span>
	</div>

	<div class="row">
		<b>生年月日</b><span class="space">${userData.birthDate}</span>
	</div>

	<div class="row">
		<b>登録日時</b><span class="space">${userData.createDate}</span>
	</div>

	<div class="row">
		<b>更新日時</b><span class="space">${userData.updateDate}</span>
	</div>
</div>


<br>
<a href="UserTableServlet"><font color="blue">戻る</font></a>

</body>
</html>