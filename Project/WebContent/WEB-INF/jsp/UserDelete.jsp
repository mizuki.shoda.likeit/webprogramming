<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除確認</title>
</head>
<body>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">

<link href="css/userDelete.css" rel="stylesheet" type="text/css" />


<div class=text-area>
${userInfo.name}さん
<a href="UserLogoutServlet"><font color="red">ログアウト</font></a>
</div>

<h1 align="center">ユーザー削除確認</h1><br>

<div class="delete-area">
${userData.loginId}<br>
を本当に削除してよろしいでしょうか。
<br>
<br>
<br>
<div class="row">
	<a href="UserTableServlet"><input type="button" class="btn btn-primary" value="キャンセル"></a>

		<form action="UserDeleteServlet" method="post">
			<span class="space">
				<input type="submit" class="btn btn-primary" value="OK">
			</span>
				<input type="hidden" name="id" name="id" value="${userData.id}">
		</form>
</div>
</div>

</body>
</html>