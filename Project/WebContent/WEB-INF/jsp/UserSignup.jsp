<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
</head>

<body>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">

<link href="css/userSignup.css" rel="stylesheet" type="text/css" />

<div class=text-area>
${userInfo.name}さん
<a href="UserLogoutServlet"><font color="red">ログアウト</font></a>
</div>

<h1 align="center">ユーザー新規登録</h1><br>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<c:if test="${errMsg2 != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg2}
		</div>
	</c:if>


<form action="UserSignupServlet" method="post">

<div class=signup-area>

	<div class="row">
		<b class=col-4>ログインID</b>
		<input type="text" name="loginId" class="form-control col-8" placeholder="半角英数字" required>
	</div>

	<div class="row">
		<b class=col-4>パスワード </b>
		<input type="text" name="password" class="form-control col-8" placeholder="半角英数字" required>
	</div>

	<div class="row">
		<b class=col-4>パスワード(確認)</b>
		<input type="password" name="confirmPassword" class="form-control col-8" placeholder="確認の為、再入力してください" required>
	</div>

	<div class="row">
		<b class=col-4>ユーザー名</b>
		<input type="text" name="userName" class="form-control col-8" placeholder="全角" required>
	</div>

	<div class="row">

		<b class=col-4>生年月日</b> <input type="date" name="birthDate" class="form-control col-8" required>
	</div>
</div>
<br>
<br>
	<div class=button-area>
	<input type="submit" class="btn btn-primary" value="登録">
	</div>
</form>
<br>
<br>

		<a href="UserTableServlet"><font color="blue">戻る</font></a>

</body>
</html>