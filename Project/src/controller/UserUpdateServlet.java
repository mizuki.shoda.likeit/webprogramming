package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session =request.getSession();
		User userInfo=(User)session.getAttribute("userInfo");

		if(userInfo == null){
			response.sendRedirect("UserLoginServlet");
			return;
		}

		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.searchUser(id);

		request.setAttribute("userData",user);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id=request.getParameter("id");
		String password=request.getParameter("password");
		String confirmPassword=request.getParameter("confirmPassword");
		String userName=request.getParameter("userName");
		String birthDate=request.getParameter("birthDate");

		if (!(password.equals(confirmPassword))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			request.setAttribute("inputName",userName);
			request.setAttribute("inputBirthDate", birthDate);

			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		UserDao userDao = new UserDao();

		if (password.equals("")) {
			userDao.updateUser2(id,userName,birthDate);
			response.sendRedirect("UserTableServlet");
			return;
		}

		userDao.updateUser(id,password,userName,birthDate);

		response.sendRedirect("UserTableServlet");
	}

}
