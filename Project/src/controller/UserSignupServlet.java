package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserSignupServlet
 */
@WebServlet("/UserSignupServlet")
public class UserSignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSignupServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session=request.getSession();
		User userInfo=(User)session.getAttribute("userInfo");

		if(userInfo == null){
			response.sendRedirect("UserLoginServlet");
			return;
		}

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/UserSignup.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId=request.getParameter("loginId");
		String password=request.getParameter("password");
		String confirmPassword=request.getParameter("confirmPassword");
		String userName=request.getParameter("userName");
		String birthDate=request.getParameter("birthDate");

		if (!(password.equals(confirmPassword))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			request.setAttribute("inputLoginID", loginId);
			request.setAttribute("inputName",userName);
			request.setAttribute("inputBirthDate", birthDate);


			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/UserSignup.jsp");
			dispatcher.forward(request, response);
			return;
		}

		UserDao userDao = new UserDao();
		User searchId=userDao.searchId(loginId);

		if(searchId!=null) {
			request.setAttribute("errMsg2", "既に登録済みのログインIDです");

			request.setAttribute("inputLoginID", loginId);
			request.setAttribute("inputName",userName);
			request.setAttribute("inputBirthDate", birthDate);

			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/UserSignup.jsp");
			dispatcher.forward(request, response);
			return;
		}
		userDao.addUser(loginId, userName, birthDate, password);


		response.sendRedirect("UserTableServlet");
	}

}
