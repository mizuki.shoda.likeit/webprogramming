package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserTableServlet
 */
@WebServlet("/UserTableServlet")
public class UserTableServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserTableServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session =request.getSession();
		User userInfo=(User)session.getAttribute("userInfo");

		if(userInfo == null){
			response.sendRedirect("UserLoginServlet");
			return;
		}


		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/UserTable.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId=request.getParameter("loginId");
		String userName=request.getParameter("userName");
		String startBirthDate=request.getParameter("startBirthDate");
		String endBirthDate=request.getParameter("endBirthDate");

		UserDao userDao = new UserDao();
		List<User> userList = userDao.findUser(loginId, userName, startBirthDate,  endBirthDate);

		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/UserTable.jsp");
		dispatcher.forward(request, response);

	}

}
